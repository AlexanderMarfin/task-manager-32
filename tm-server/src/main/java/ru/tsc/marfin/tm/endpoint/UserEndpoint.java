package ru.tsc.marfin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.api.endpoint.IUserEndpoint;
import ru.tsc.marfin.tm.api.service.IServiceLocator;
import ru.tsc.marfin.tm.api.service.IUserService;
import ru.tsc.marfin.tm.dto.request.*;
import ru.tsc.marfin.tm.dto.response.*;
import ru.tsc.marfin.tm.enumerated.Role;
import ru.tsc.marfin.tm.model.User;

public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    public UserChangePasswordResponse changeUserPassword(@NotNull final UserChangePasswordRequest request) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable String password = request.getPassword();
        @Nullable User user = getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    public UserLockResponse lockUser(@NotNull final UserLockRequest request) {
        check(request, Role.ADMIN);
        @Nullable String login = request.getLogin();
        getUserService().lockUserByLogin(login);
        return new UserLockResponse();
    }

    @NotNull
    @Override
    public UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request) {
        check(request);
        @Nullable String login = request.getLogin();
        @Nullable String password = request.getPassword();
        @Nullable String email = request.getEmail();
        @Nullable User user = getUserService().create(login, password, email);
        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    public UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request) {
        check(request, Role.ADMIN);
        @Nullable String login = request.getLogin();
        getUserService().removeByLogin(login);
        return new UserRemoveResponse();
    }

    @NotNull
    @Override
    public UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request) {
        check(request, Role.ADMIN);
        @Nullable String login = request.getLogin();
        getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    public UserUpdateProfileResponse updateUserProfile(@NotNull final UserUpdateProfileRequest request) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable String lastName = request.getLastName();
        @Nullable String firstName = request.getFirstName();
        @Nullable String middleName = request.getMiddleName();
        @Nullable User user = getUserService().updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateProfileResponse(user);
    }

}
