package ru.tsc.marfin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.marfin.tm.api.endpoint.*;
import ru.tsc.marfin.tm.api.repository.IProjectRepository;
import ru.tsc.marfin.tm.api.repository.ITaskRepository;
import ru.tsc.marfin.tm.api.repository.IUserRepository;
import ru.tsc.marfin.tm.api.service.*;
import ru.tsc.marfin.tm.dto.request.*;
import ru.tsc.marfin.tm.endpoint.*;
import ru.tsc.marfin.tm.enumerated.Role;
import ru.tsc.marfin.tm.repository.ProjectRepository;
import ru.tsc.marfin.tm.repository.TaskRepository;
import ru.tsc.marfin.tm.repository.UserRepository;
import ru.tsc.marfin.tm.service.*;
import ru.tsc.marfin.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @NotNull
    private  final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService();

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run() {
        initPID();
        initUsers();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutDown));
        backup.start();
        server.start();
    }

    private void prepareShutDown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN**");
        backup.stop();
        server.stop();
    }

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);

        server.registry(DataBackupLoadRequest.class, domainEndpoint::loadDataBackup);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::saveDataBackup);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::loadDataBase64);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::saveDataBase64);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::loadDataBinary);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::saveDataBinary);
        server.registry(DataJsonFasterXmlLoadRequest.class, domainEndpoint::loadDataJsonFasterXml);
        server.registry(DataJsonFasterXmlSaveRequest.class, domainEndpoint::saveDataJsonFasterXml);
        server.registry(DataJsonJaxbLoadRequest.class, domainEndpoint::loadDataJsonJaxb);
        server.registry(DataJsonJaxbSaveRequest.class, domainEndpoint::saveDataJsonJaxb);
        server.registry(DataXmlFasterXmlLoadRequest.class, domainEndpoint::loadDataXmlFasterXml);
        server.registry(DataXmlFasterXmlSaveRequest.class, domainEndpoint::saveDataXmlFasterXml);
        server.registry(DataXmlJaxbLoadRequest.class, domainEndpoint::loadDataXmlJaxb);
        server.registry(DataXmlJaxbSaveRequest.class, domainEndpoint::saveDataXmlJaxb);
        server.registry(DataYamlFasterXmlLoadRequest.class, domainEndpoint::loadDataYamlFasterXml);
        server.registry(DataYamlFasterXmlSaveRequest.class, domainEndpoint::saveDataYamlFasterXml);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeProjectStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeProjectStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::clearProject);
        server.registry(ProjectCreateRequest.class, projectEndpoint::createProject);
        server.registry(ProjectListRequest.class, projectEndpoint::listProject);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeProjectById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeProjectByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateProjectById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateProjectByIndex);

        server.registry(TaskBindToProjectRequest.class, projectTaskEndpoint::bindTaskToProject);
        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeTaskStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeTaskStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clearTask);
        server.registry(TaskCreateRequest.class, taskEndpoint::createTask);
        server.registry(TaskListRequest.class, taskEndpoint::listTask);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeTaskById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeTaskByIndex);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::showTaskById);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::showTaskByIndex);
        server.registry(TaskShowByProjectIdRequest.class, taskEndpoint::showTaskByProjectId);
        server.registry(TaskUnbindFromProjectRequest.class, projectTaskEndpoint::unbindTaskFromProject);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateTaskById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateTaskByIndex);

        server.registry(UserChangePasswordRequest.class, userEndpoint::changeUserPassword);
        server.registry(UserLockRequest.class, userEndpoint::lockUser);
        server.registry(UserRegistryRequest.class, userEndpoint::registryUser);
        server.registry(UserRemoveRequest.class, userEndpoint::removeUser);
        server.registry(UserUnlockRequest.class, userEndpoint::unlockUser);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::updateUserProfile);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

}
