package ru.tsc.marfin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.marfin.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.marfin.tm.api.service.IServiceLocator;
import ru.tsc.marfin.tm.dto.request.*;
import ru.tsc.marfin.tm.dto.response.*;
import ru.tsc.marfin.tm.enumerated.Role;

public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public DataBackupLoadResponse loadDataBackup(@NotNull final DataBackupLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    public DataBackupSaveResponse saveDataBackup(@NotNull final DataBackupSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    public DataBase64LoadResponse loadDataBase64(@NotNull final DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveDataBase64(@NotNull final DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse loadDataBinary(@NotNull final DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    public DataBinarySaveResponse saveDataBinary(@NotNull final DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    public DataJsonFasterXmlLoadResponse loadDataJsonFasterXml(@NotNull final DataJsonFasterXmlLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonFasterXml();
        return new DataJsonFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    public DataJsonFasterXmlSaveResponse saveDataJsonFasterXml(@NotNull final DataJsonFasterXmlSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonFasterXml();
        return new DataJsonFasterXmlSaveResponse();
    }

    @NotNull
    @Override
    public DataJsonJaxbLoadResponse loadDataJsonJaxb(@NotNull final DataJsonJaxbLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonJaxb();
        return new DataJsonJaxbLoadResponse();
    }

    @NotNull
    @Override
    public DataJsonJaxbSaveResponse saveDataJsonJaxb(@NotNull final DataJsonJaxbSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonJaxb();
        return new DataJsonJaxbSaveResponse();
    }

    @NotNull
    @Override
    public DataXmlFasterXmlLoadResponse loadDataXmlFasterXml(@NotNull final DataXmlFasterXmlLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlFasterXml();
        return new DataXmlFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    public DataXmlFasterXmlSaveResponse saveDataXmlFasterXml(@NotNull final DataXmlFasterXmlSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlFasterXml();
        return new DataXmlFasterXmlSaveResponse();
    }

    @NotNull
    @Override
    public DataXmlJaxbLoadResponse loadDataXmlJaxb(@NotNull final DataXmlJaxbLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlJaxb();
        return new DataXmlJaxbLoadResponse();
    }

    @NotNull
    @Override
    public DataXmlJaxbSaveResponse saveDataXmlJaxb(@NotNull final DataXmlJaxbSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlJaxb();
        return new DataXmlJaxbSaveResponse();
    }

    @NotNull
    @Override
    public DataYamlFasterXmlLoadResponse loadDataYamlFasterXml(@NotNull final DataYamlFasterXmlLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataYamlFasterXml();
        return new DataYamlFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    public DataYamlFasterXmlSaveResponse saveDataYamlFasterXml(@NotNull final DataYamlFasterXmlSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataYamlFasterXml();
        return new DataYamlFasterXmlSaveResponse();
    }

}
