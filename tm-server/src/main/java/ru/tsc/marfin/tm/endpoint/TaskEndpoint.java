package ru.tsc.marfin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.marfin.tm.api.service.IServiceLocator;
import ru.tsc.marfin.tm.api.service.ITaskService;
import ru.tsc.marfin.tm.dto.request.*;
import ru.tsc.marfin.tm.dto.response.*;
import ru.tsc.marfin.tm.enumerated.Sort;
import ru.tsc.marfin.tm.enumerated.Status;
import ru.tsc.marfin.tm.model.Task;

import java.util.List;

public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @NotNull
    @Override
    public TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull final TaskChangeStatusByIdRequest request) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable String id = request.getId();
        @Nullable Status status = request.getStatus();
        @Nullable Task task = getTaskService().changeTaskStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(
            @NotNull final TaskChangeStatusByIndexRequest request
    ) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable Integer index = request.getIndex();
        @Nullable Status status = request.getStatus();
        @Nullable Task task = getTaskService().changeTaskStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskClearResponse clearTask(@NotNull final TaskClearRequest request) {
        check(request);
        @Nullable String userId = request.getUserId();
        getTaskService().clear(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    public TaskCreateResponse createTask(@NotNull final TaskCreateRequest request) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable String name = request.getName();
        @Nullable String description = request.getDescription();
        @Nullable Task task = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    public TaskListResponse listTask(@NotNull final TaskListRequest request) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable Sort sort = request.getSort();
        @Nullable List<Task> tasks = getTaskService().findAll(sort);
        return new TaskListResponse(tasks);
    }

    @NotNull
    @Override
    public TaskRemoveByIdResponse removeTaskById(@NotNull final TaskRemoveByIdRequest request) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable String id = request.getUserId();
        @Nullable Task task = getTaskService().removeById(userId, id);
        return new TaskRemoveByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskRemoveByIndexResponse removeTaskByIndex(@NotNull final TaskRemoveByIndexRequest request) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable Integer index = request.getIndex();
        @Nullable Task task = getTaskService().removeByIndex(userId, index);
        return new TaskRemoveByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskShowByIdResponse showTaskById(@NotNull final TaskShowByIdRequest request) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable String id = request.getId();
        @Nullable Task task = getTaskService().findOneById(userId, id);
        return new TaskShowByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskShowByIndexResponse showTaskByIndex(@NotNull final TaskShowByIndexRequest request) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable Integer index = request.getIndex();
        @Nullable Task task = getTaskService().findOneByIndex(userId, index);
        return new TaskShowByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskShowByProjectIdResponse showTaskByProjectId(@NotNull final TaskShowByProjectIdRequest request) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable String projectId = request.getProjectId();
        @Nullable List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        return new TaskShowByProjectIdResponse(tasks);
    }

    @NotNull
    @Override
    public TaskUpdateByIdResponse updateTaskById(@NotNull final TaskUpdateByIdRequest request) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable String id = request.getId();
        @Nullable String name = request.getName();
        @Nullable String description = request.getDescription();
        @Nullable Task task = getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskUpdateByIndexResponse updateTaskByIndex(@NotNull final TaskUpdateByIndexRequest request) {
        check(request);
        @Nullable String userId = request.getUserId();
        @Nullable Integer index = request.getIndex();
        @Nullable String name = request.getName();
        @Nullable String description = request.getDescription();
        @Nullable Task task = getTaskService().updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

}
