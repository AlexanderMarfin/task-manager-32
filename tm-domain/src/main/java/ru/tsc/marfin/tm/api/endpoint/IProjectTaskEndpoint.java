package ru.tsc.marfin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.marfin.tm.dto.request.TaskBindToProjectRequest;
import ru.tsc.marfin.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.tsc.marfin.tm.dto.response.TaskBindToProjectResponse;
import ru.tsc.marfin.tm.dto.response.TaskUnbindFromProjectResponse;

public interface IProjectTaskEndpoint {

    @NotNull
    TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request);

}
