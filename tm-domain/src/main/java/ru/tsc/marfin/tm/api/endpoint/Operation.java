package ru.tsc.marfin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.marfin.tm.dto.request.AbstractRequest;
import ru.tsc.marfin.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(@NotNull RQ request);

}
