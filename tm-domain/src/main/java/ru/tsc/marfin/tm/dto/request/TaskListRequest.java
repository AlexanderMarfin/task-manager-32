package ru.tsc.marfin.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sort;

}
