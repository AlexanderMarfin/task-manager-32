package ru.tsc.marfin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.marfin.tm.dto.request.ServerAboutRequest;
import ru.tsc.marfin.tm.dto.request.ServerVersionRequest;
import ru.tsc.marfin.tm.dto.response.ServerAboutResponse;
import ru.tsc.marfin.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}
