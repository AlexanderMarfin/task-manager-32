package ru.tsc.marfin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractModel implements Serializable {

    private static final long serialVersionUID = -558170197280947376L;

    @NotNull
    private String id = UUID.randomUUID().toString();

}
