package ru.tsc.marfin.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.marfin.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.marfin.tm.api.endpoint.IEndpointClient;
import ru.tsc.marfin.tm.dto.request.UserLoginRequest;
import ru.tsc.marfin.tm.dto.request.UserLogoutRequest;
import ru.tsc.marfin.tm.dto.request.UserShowProfileRequest;
import ru.tsc.marfin.tm.dto.response.UserLoginResponse;
import ru.tsc.marfin.tm.dto.response.UserLogoutResponse;
import ru.tsc.marfin.tm.dto.response.UserShowProfileResponse;

@NoArgsConstructor
public class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpoint, IEndpointClient {

    public AuthEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserShowProfileResponse profile(@NotNull final UserShowProfileRequest request) {
        return call(request, UserShowProfileResponse.class);
    }

}
