package ru.tsc.marfin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.dto.request.UserShowProfileRequest;
import ru.tsc.marfin.tm.dto.response.UserShowProfileResponse;
import ru.tsc.marfin.tm.enumerated.Role;
import ru.tsc.marfin.tm.exception.entity.UserNotFoundException;
import ru.tsc.marfin.tm.model.User;

public final class UserShowProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-show-profile";

    @NotNull
    public static final String DESCRIPTION = "Show user profile";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[SHOW USER PROFILE]");
        @NotNull UserShowProfileResponse response = serviceLocator.getAuthEndpoint().profile(new UserShowProfileRequest());
        @Nullable final User user = response.getUser();
        if (user == null) throw new UserNotFoundException();
        showUser(user);
    }

}
