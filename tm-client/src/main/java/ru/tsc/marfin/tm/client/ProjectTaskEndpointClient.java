package ru.tsc.marfin.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.marfin.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.marfin.tm.dto.request.TaskBindToProjectRequest;
import ru.tsc.marfin.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.tsc.marfin.tm.dto.response.TaskBindToProjectResponse;
import ru.tsc.marfin.tm.dto.response.TaskUnbindFromProjectResponse;

public class ProjectTaskEndpointClient extends AuthEndpointClient implements IProjectTaskEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public TaskBindToProjectResponse bindTaskToProject(@NotNull final TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull final TaskUnbindFromProjectRequest request) {
        return call(request, TaskUnbindFromProjectResponse.class);
    }

}
