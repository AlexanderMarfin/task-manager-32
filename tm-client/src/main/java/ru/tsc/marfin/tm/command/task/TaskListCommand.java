package ru.tsc.marfin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.dto.request.TaskListRequest;
import ru.tsc.marfin.tm.dto.response.TaskListResponse;
import ru.tsc.marfin.tm.enumerated.Sort;
import ru.tsc.marfin.tm.model.Task;
import ru.tsc.marfin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-list";

    @NotNull
    public static final String DESCRIPTION = "Show task list";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final Sort sort = Sort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest(sort);
        @NotNull final TaskListResponse response = getTaskEndpoint().listTask(request);
        @Nullable final List<Task> tasks = response.getTasks();
        renderTasks(tasks);
    }

}
