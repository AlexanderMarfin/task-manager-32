package ru.tsc.marfin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.dto.request.UserRegistryRequest;
import ru.tsc.marfin.tm.enumerated.Role;
import ru.tsc.marfin.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-registry";

    @NotNull
    public static final String DESCRIPTION = "Registry user";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        getUserEndpoint().registryUser(new UserRegistryRequest(login, password, email));
    }

}
