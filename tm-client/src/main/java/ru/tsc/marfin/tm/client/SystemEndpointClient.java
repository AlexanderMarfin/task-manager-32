package ru.tsc.marfin.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.marfin.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.marfin.tm.dto.request.ServerAboutRequest;
import ru.tsc.marfin.tm.dto.request.ServerVersionRequest;
import ru.tsc.marfin.tm.dto.response.ServerAboutResponse;
import ru.tsc.marfin.tm.dto.response.ServerVersionResponse;

public class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return call(request, ServerAboutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return call(request, ServerVersionResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NotNull final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(serverAboutResponse.getEmail());
        System.out.println(serverAboutResponse.getName());
        @NotNull final ServerVersionResponse serverVersionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(serverVersionResponse.getVersion());
        client.disconnect();
    }

}
